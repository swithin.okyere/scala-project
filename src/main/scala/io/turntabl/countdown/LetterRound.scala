
package io.turntabl.countdown

import scala.annotation.tailrec
import scala.util.Random
import scala.io.StdIn.readLine
import scala.io.Source

class LetterRound{
  val vowelList: List[Char] = List('a', 'e', 'i', 'o', 'u')
  val vowelFrequency: List[Int] = List(9,12,9,8,4)
  val consonantList: List[Char] = List('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z')
  val consonantFrequency: List[Int] = List(2,2,4,2,3,2,1,1,4,2,6,2,1,6,4,6,2,2,1,2,1)

  val newVowel: List[(Char, Int)] = vowelList.zip(vowelFrequency)
  val newConsonant: List[(Char, Int)] = consonantList.zip(consonantFrequency)


  val finalVowelList: List[List[Char]] = for {
    x <- newVowel
  } yield List.fill(x._2)(x._1)

  val finalConsonantList: List[List[Char]] = for {
    x <- newConsonant
  } yield List.fill(x._2)(x._1)


  var randomVowels = Random.shuffle(finalVowelList.flatten)
  var randomConsonants = Random.shuffle(finalConsonantList.flatten)


//  println(randomVowels)
//  println(randomConsonants)
//  def randomise(list: List[Char]) = Random.shuffle(list)

  def generateVowel(): Char ={
    val getOne = randomVowels.head
    randomVowels = randomVowels.tail
    getOne.toChar
  }

  def generateConsonant(): Char = {
    val getOne = randomConsonants.head
    randomConsonants = randomConsonants.tail
    getOne.toChar
  }

  def letterChoice(n: Int = 9): List[Char]  ={

    def getLetter(acc:List[Char], vowelCount:Int, consonantCount:Int): List[Char] = {
      if ( acc.size == 9) {
        println(s"Players have this list of letters to form a word: $acc")
        acc
      }
      else {
        print("Which letter do you want... (v for vowel, c for consonant): ")
        var playerInput = readLine()
        if (playerInput.equals("v")) {
          if(vowelCount >= 5 ){
            println("max vowel count, please use a consonant")
            getLetter(acc, vowelCount, consonantCount)
          }
          else getLetter(acc :+ generateVowel(), vowelCount+1, consonantCount)
        } else if (playerInput.equals("c")) {
          if (consonantCount >= 6){
            println("max consonant count, please use a vowel")
            getLetter(acc, vowelCount, consonantCount)
          }
          else getLetter(acc :+ generateConsonant(), vowelCount , consonantCount + 1)
        } else {
          println("Wrong input. Enter either v or c: ")
          getLetter(acc, vowelCount, consonantCount)
        }
      }
    }

    getLetter(List(), 0, 0)
  }
//  def returnLetters(): Unit ={
//    val playerVowels = randomVowels.slice(0, 4)
//    val playerConsonants = randomConsonants.slice(0,5)
//    val playerLetters = playerVowels.concat(playerConsonants)
////    println(playerLetters)
//  }

  def checkIfWordExists(contestant: Contestant, word: String) = {
    val filename = "C:\\Users\\swithin.okyere\\IdeaProjects\\scala-project\\src\\main\\scala\\io\\turntabl\\countdown\\valid_words.txt"
    val fileContents = Source.fromFile(filename).getLines()
    if(fileContents.contains(word)){
      println("Word found")
      if(word.length == 9){
        contestant.points = contestant.points + 18
        println(s"${contestant.name} has ${contestant.points} points")
      } else {
        contestant.points = contestant.points + word.length
        println(s"${contestant.name} has ${contestant.points} points")
      }
    }

  }


  def gamePlay(): Unit ={

    println("--------------------------Welcome to Round One [Letter Round]!------------------------------- ")
    println()

    print("Player one, enter name: ")
    val playerOneName = readLine()
    print("Player two, enter name: ")
    val playerTwoName = readLine()


    val playerOne = Contestant(playerOneName, 0)
    val playerTwo = Contestant(playerTwoName, 0)
    println()

    println("Please select 9 letters with a minimum of 3 vowels and minimum of 4 consonants")
    letterChoice()

//    print("Players have this list of letters to form a word: ")
//    println(acc)

    //returnLetters()

    print(s"Please enter a word, $playerOneName: ")
    val playerOneWord = readLine()

    print(s"Please enter a word, $playerTwoName: ")
    val playerTwoWord = readLine()

    checkIfWordExists(playerOne,playerOneWord)
    checkIfWordExists(playerTwo,playerTwoWord)


  }

}

object LetterRound{
  def main(args: Array[String]): Unit = {
    val letterRound = new LetterRound()
    letterRound.gamePlay()
  }
}

