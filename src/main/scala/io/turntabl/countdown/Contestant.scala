package io.turntabl.countdown

class Contestant(val name: String, var points: Int)

object Contestant{
  def apply(name: String, points: Int): Contestant = {
    new Contestant(name, points)
  }
}
